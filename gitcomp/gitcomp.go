package gitcomp

import (
	"strings"

	"github.com/c-bata/go-prompt"
)

// TODO Ask Philippe Blain what he things
// about these attributes
// An orthodonal basis for the space of possible
type GitCompleter struct {
	subcommand    string
	d             prompt.Document
	argsSituation ArgsSituation
	repoState     RepoState
}

// Posargsituation represents the state of the command
// line to guide decisions about what to offer as completions.
// The constants can be OR'd together to form conditions
type ArgsSituation int

const (
	PInvalidSituation    ArgsSituation = iota
	PExpectingSubcommand               = 1 << (iota)
	PExpectingBranchName
	PExpectingRemote
	PExpectingRemoteBranch
	PExpectingFilename
	PPreviousIsOption
	PCurrentIsOption
)

// RepoState represents information about the repository
type RepoState int

// I'm not sure yet what I thing about having thes ORABLE
// I should just make them one single list otherwise it
// might start getting them confusing a & (b | c)
const (
	// InvalidState : We don't know what is the situation
	SInvalidState RepoState = iota
	// SClean : no modified tracked files
	SClean = 1 << (iota + 32)
	// SDirty : There are modified tracked files present
	SDirty
	// SUntracked : There are untracked (and not .gitignored) files
	SUntracked
	// SHeadless : Repo is in headless state
	SHeadless
	// SRebasing : There is a rebase in progress
	SRebasing
	SMerging
	// Ask Philippe Blain for more states
	// You wouldn't propose --continue unless (subcommand == rebase) && (repostate == Rebasing)
)

// GitSubCommands lists the completion info for git subcommands
var GitSubCommands = []prompt.Suggest{
	{Text: "commit", Description: "Create a node in the graph of commits"},
	{Text: "rebase", Description: "Replay changes on another version"},
}

var GitGeneralOptions = []prompt.Suggest{
	{Text: "--verbose", Description: "Add verbosity to output"},
}

// ========================== Interface ============================

func GetCompletions(d prompt.Document) []prompt.Suggest {
	return prompt.FilterFuzzy(getCompletionsImpl(d), d.GetWordBeforeCursor(), true)
}

// ============================Implementation ======================

// GetCompletions returns the completions for the current document
// Main function.  Dispatches control to complete generalOptions,
// general option arguments or the subcommand handler
func getCompletionsImpl(d prompt.Document) []prompt.Suggest {

	g := GitCompleter{d: d}
	// From experience doing autocompletions, it pays to decouple
	// getting the situation from determining what the appropriate
	// completions are given the situation
	g.argsSituation, g.repoState = g.getSituations()

	// Switch based on situation
	if (g.argsSituation & PExpectingSubcommand) != 0 {
		// Bad separation of concern, why am I doing string comparisons
		// That should go in the getSitautions
		if (g.argsSituation & PCurrentIsOption) != 0 {
			return g.completeGeneralOptions()
		}
		return g.getSubcommandCompletions()
	} else if g.subcommand != "" {
		return g.completeSubcommand()
	}
	return nil
}

// Encapsulates the knowledge of what subcommands are possible
// given the current repo state
func (g *GitCompleter) getSubcommandCompletions() []prompt.Suggest {
	switch g.repoState {
	// case SRebasing:
	// commit, rebase, what else? status, not merge, push and fetch would be possible technically
	// maybe check other factors
	default:
		return GitSubCommands
	}
}

// DIspatches to the subcommand handler based on the subcommand
func (g *GitCompleter) completeSubcommand() []prompt.Suggest {
	switch g.subcommand {
	case "commit":
		return g.completeCommitCommand()
	case "rebase":
		return g.completeRebaseCommand()
	case "branch":
		return g.completeBranchCommand()
	default:
		return nil
	}
}

// Encapsulates the knowledge of general options
// It could chane behavior based on the repo state and
// other factors but it does not
func (g *GitCompleter) completeGeneralOptions() []prompt.Suggest {
	return GitGeneralOptions
}

// Encapsulates the knowledge of how to complete commands if the
// subcommand is 'commit'
func (g *GitCompleter) completeCommitCommand() []prompt.Suggest {
	return nil
}

// Encapsulates the knowledge of how to complete commands if the
// subcommand is 'rebase'
func (g *GitCompleter) completeRebaseCommand() []prompt.Suggest {
	return nil
}

// Encapsulates the knowledge of how to complete commands if the
// subcommand is 'branch'
func (g *GitCompleter) completeBranchCommand() []prompt.Suggest {
	return nil
}

// GetSituation
func (g *GitCompleter) getSituations() (ArgsSituation, RepoState) {
	// Possible situations
	// - Have or don't have subcommand
	//    -> Scan whole line
	// - Completing General Option
	//    -> !haveSubcommand && currentWord starts with "-"
	// - Expecting Branch name
	// - Expecting Remote name
	// - Expecting XX
	var situation ArgsSituation = PExpectingSubcommand
	if strings.HasPrefix(g.d.GetWordBeforeCursor(), "-") {
		// TODO argsSituation should just be a struct with a bunch of bools
		// This flag thing with the bit stuff is really not appropriate
		// for this situation.
		situation |= PCurrentIsOption
	}

	// Not mutually exclusive, we could be expecting a branch name because
	// we are at the third posarg of 'git push origin ___' but then they
	// type a '-' so we are "expecting a branch name" but we are also
	// completing options

	// Example Situation : 'git push origin _'
	// - Expecting Branch
	return situation, SClean
}
