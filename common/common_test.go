package common

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"testing"

	"gopkg.in/yaml.v2"
)

func TestDumpToJson(t *testing.T) {
	// Noice!  TODO: Replace the go file with a
	// JSON file and some sweet unmarshaling!
	bytes, err := json.MarshalIndent(ExampleOptions, "", "    ")
	if err != nil {
		t.FailNow()
	}
	ioutil.WriteFile("options.json", bytes, 0644)
}

func TestFromJson(t *testing.T) {
	bytes, err := ioutil.ReadFile("options.json")
	if err != nil {
		t.FailNow()
	}
	var options map[string]ProgramOption
	json.Unmarshal(bytes, &options)
	fmt.Println(options)
	bytes, err = json.MarshalIndent(options, "", "    ")
	if err != nil {
		t.FailNow()
	}
	ioutil.WriteFile("options_rewrite.json", bytes, 0644)
	bytes, err = yaml.Marshal(ExampleOptions)
	if err != nil {
		t.FailNow()
	}
	ioutil.WriteFile("options.yaml", bytes, 0644)
}
