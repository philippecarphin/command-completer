package common

// How does one define the way their program should be completed
// Lets say a typical program will have
import (
	"github.com/c-bata/go-prompt"
)

// ProgramOption defines the info concerning
// an option of a program.  One can define a
// complter by defining a slice of these structs
type ProgramOption struct {
	Option      string
	HasArgs     bool
	Description string
	Candidates  []prompt.Suggest
	// OtherComplete : A string like "filesystem", and possibly
	// other options.  For example From a director
	// or only python files or a couple of other things.
	// We can make it
	CompleteType int               // filesystme : normal | directory | filetype | ...
	CompleteArgs map[string]string // dir:<dirtosearch> , filetype:<filetype> or whatever
	// Number (the suggestion should just say Number with an example like Example 10G for memory)
}
