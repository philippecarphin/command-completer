package common

import (
	"github.com/c-bata/go-prompt"
)


// Options of the program
// This is an example of options describing a program
// TODO Put this in example
var ExampleOptions = map[string]ProgramOption{
	"-cpus": {
		Option:      "-ncpu",
		Description: "Number of CPUS",
		HasArgs:     true,
		Candidates: []prompt.Suggest{
			{Text: "1", Description: "One core, veeeery small"},
			{Text: "10", Description: "More cores"},
			{Text: "40", Description: "A full node"},
		},
	},
	"-queue": {
		Option:      "-queue",
		HasArgs:     true,
		Description: "The Queue that it's running on",
		Candidates: []prompt.Suggest{
			{Text: "dev", Description: "The queue for the ___ group"},
			{Text: "dev_daemon", Description: "The queue for long running stuff"},
			{Text: "prod_daemon", Description: "The queue for the ___ group"},
		},
	},
	"-verbose": {
		Option:      "-verbose",
		Description: "Show verbose output",
		HasArgs:     false,
		Candidates:  nil,
	},
}

// TODO Also put this in example
var fileinfo = map[string]string{
	".py":      "Python files for use with the Python interpreter",
	".sh":      "Shell script, consider using a python script instead",
	".go":      "Source code for the go programming language",
	"Makefile": "Source file for the Make build system",
	".txt":     "Plain text file",
}

// TODO Put in example
var specialFiles = map[string]string{
	".profile":   "The profile file that defines your shell environment",
	".git":       "Don't touch this, this is contains git implementation details for your repo",
	".gitignore": "IMPORTANT: Use this to ignore build files",
}

