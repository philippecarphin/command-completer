package common

import (
	"fmt"
	"io/ioutil"
	"os"
	"strings"

	"github.com/c-bata/go-prompt"
	"github.com/google/shlex"
)

// GeneralCompleter General things that we might want to know everywhere
// TODO Remove useless attributes
// This will be refined to allow defining a completer
// EXCLUSIVELY from the structs above marked as
// like the structs above in the form of a JSON file
type GeneralCompleter struct {
	doc              prompt.Document
	optionToComplete string
	haveSubcommand   bool
	subcommand       string
	argv             []string
	command          string
	currentWord      string
	previousWord     string
	Options          map[string]ProgramOption
}

// Situation describes what type of logic we are following
// Implementation detail of example completer
type Situation int

const (
	errorSituation = iota
	completingOption
	completingOptarg
	completingPosarg
	noCompletion // like after "--"
)

// GetGeneralCompleter based on Options from the
// the map defining the options for the program
func GetGeneralCompleter(opts map[string]ProgramOption) GeneralCompleter {
	comp := GeneralCompleter{}
	comp.Options = opts
	return comp
}

// GetCompletions returns completions for MyProgram
func (e *GeneralCompleter) GetCompletions(d prompt.Document) []prompt.Suggest {

	e.argv, _ = shlex.Split(d.CurrentLine())

	e.command = e.argv[0]
	if len(e.argv) >= 3 {
		e.previousWord = e.argv[len(e.argv)-2]
	}
	e.currentWord = e.argv[len(e.argv)-1]

	situation, _ := e.getSituation()

	candidates := e.getCandidates(situation)

	return prompt.FilterHasPrefix(candidates, d.GetWordBeforeCursor(), true)
}

func (e *GeneralCompleter) getCandidates(situation Situation) []prompt.Suggest {
	switch situation {
	case completingOption:
		return e.completeOption()
	case completingOptarg:
		return e.completeOptarg()
	case completingPosarg:
		return e.completePosarg()
	default:
		return nil
	}
}

func (e *GeneralCompleter) getSituation() (sit Situation, err error) {
	for _, w := range e.argv {
		if w == "--" {
			// Maybe could have a situation completeFilesystem
			// return noCompletion, nil
			return completingPosarg, nil
		}
	}
	if len(e.argv) == 2 {
		// Maybe we would want to complete a subcommand here
		sit, err = completingOption, nil
	} else if strings.HasPrefix(e.previousWord, "-") && e.Options[e.previousWord].HasArgs {
		e.optionToComplete = e.previousWord
		sit, err = completingOptarg, nil
	} else if strings.HasPrefix(e.currentWord, "-") {
		// See ord_soumet for a more complex decision
		// tree to decide to go into option completion
		sit, err = completingOption, nil
	} else {
		sit, err = completingPosarg, nil
	}
	return sit, nil
}
func (e *GeneralCompleter) completeOption() []prompt.Suggest {
	candidates := []prompt.Suggest{}
	for k, v := range e.Options {
		cand := prompt.Suggest{Text: k, Description: v.Description}
		candidates = append(candidates, cand)
	}
	return candidates
}

func (e *GeneralCompleter) completeOptarg() []prompt.Suggest {
	for k, v := range e.Options {
		if e.previousWord == k {
			return v.Candidates
		}
	}
	return nil
}

func (e *GeneralCompleter) completePosarg() []prompt.Suggest {
	cwd, _ := os.Getwd()
	files, err := ioutil.ReadDir(cwd)
	if err != nil {
		return nil
	}

	suggestions := []prompt.Suggest{}
	for _, f := range files {
		sugg := prompt.Suggest{
			Text:        f.Name(),
			Description: getFileDescription(f),
		}
		suggestions = append(suggestions, sugg)
	}
	return suggestions
}

func getFileDescription(f os.FileInfo) string {

	for specialFile, desc := range specialFiles {
		if f.Name() == specialFile {
			return desc
		}
	}

	for suffix, desc := range fileinfo {
		if strings.HasSuffix(f.Name(), suffix) {
			return desc
		}
	}
	var desc string
	if f.IsDir() {
		desc = "Directory"
	} else if f.Mode().IsRegular() {
		desc = "Regular file"
	} else {
		target, _ := os.Readlink(f.Name())
		desc = fmt.Sprintf("link -> %s", target)
	}
	return desc
}
