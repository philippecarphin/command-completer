package main

import (
	"fmt"
	"log"
	"os"
	"os/exec"
	"strings"

	"github.com/c-bata/go-prompt"
	"gitlab.com/philippecarphin/command-completer/common"
	"gitlab.com/philippecarphin/command-completer/gitcomp"
)

// Registry of commands Names
var cmcCommands = []prompt.Suggest{
    {Text: "general", Description: "Example built from json"},
	{Text: "git", Description: "The stupid content tracker by Linus Torvalds"},
}

func completer(d prompt.Document) []prompt.Suggest {

	words := strings.Split(d.CurrentLine(), " ")

	if len(words) == 1 {
		return prompt.FilterHasPrefix(cmcCommands, d.GetWordBeforeCursor(), true)
	}

	switch words[0] {
	case "general":
		comp := common.GetGeneralCompleter(common.ExampleOptions)
		return comp.GetCompletions(d)
	case "git":
		return gitcomp.GetCompletions(d)
	default:
		return nil
	}
}

func main() {

	f := configureFileLogging("log.txt")
	defer f.Close()

	fmt.Println(`Please start to type a cmc command like
git or general , ...
(exit, C-c, C-d work similarily to BASH)`)

	fgGreen := prompt.OptionPrefixTextColor(prompt.Green)
	for {
		t := prompt.Input("cmc:science> ", completer, fgGreen)
		if t == "exit" || t == "" {
			break
		}
		runCommand(t)
	}
}

func runCommand(command string) {
	fmt.Printf("Running 'bash -c \"%s\"\n", command)
	c := exec.Command("bash", "-c", command)
	c.Stdout = os.Stdout
	c.Stderr = os.Stderr
	c.Run()
}


func configureFileLogging(file string) *os.File {
	f, err := os.OpenFile(file, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	if err != nil {
		log.Fatalf("error opening file: %v", err)
	}
	log.Printf("Redirecting logging to file %s for interactive process\n", file)
	log.SetOutput(f)
	return f
}
