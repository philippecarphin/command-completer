package main

import (
	"log"
	"time"
)

func generator(n int, outCh chan int) {
	for i := 0; i < n; i++ {
		log.Printf("Yielding %d\n", i)
		outCh <- i
		time.Sleep(time.Millisecond * 1)
	}
	close(outCh)
}

func main() {

	outCh := make(chan int, 0)
	go generator(8, outCh)
	for i := range outCh {
		log.Printf("Main loop : i = %d", i)
	}
}
